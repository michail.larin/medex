﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Medex.Persistence.Migrations
{
    public partial class AddDistributorPrice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DistributorPrices",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DistributorId = table.Column<long>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    PublicDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    DocumentId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistributorPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DistributorPrices_Distributors_DistributorId",
                        column: x => x.DistributorId,
                        principalTable: "Distributors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DistributorPrices_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DistributorPriceItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DistributorPriceId = table.Column<long>(nullable: false),
                    ProductId = table.Column<long>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Cost = table.Column<decimal>(nullable: false),
                    CostInDollar = table.Column<decimal>(nullable: false),
                    CostInEuro = table.Column<decimal>(nullable: false),
                    Margin = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistributorPriceItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DistributorPriceItems_DistributorPrices_DistributorPriceId",
                        column: x => x.DistributorPriceId,
                        principalTable: "DistributorPrices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DistributorPriceItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DistributorPriceItems_DistributorPriceId",
                table: "DistributorPriceItems",
                column: "DistributorPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributorPriceItems_ProductId",
                table: "DistributorPriceItems",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributorPrices_DistributorId",
                table: "DistributorPrices",
                column: "DistributorId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributorPrices_DocumentId",
                table: "DistributorPrices",
                column: "DocumentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DistributorPriceItems");

            migrationBuilder.DropTable(
                name: "DistributorPrices");
        }
    }
}
