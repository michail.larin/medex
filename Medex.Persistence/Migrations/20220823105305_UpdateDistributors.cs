﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Medex.Persistence.Migrations
{
    public partial class UpdateDistributors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Distributors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Details",
                table: "Distributors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Distributors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Fax",
                table: "Distributors",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Distributors",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "LegalName",
                table: "Distributors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Distributors",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Distributors");

            migrationBuilder.DropColumn(
                name: "Details",
                table: "Distributors");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Distributors");

            migrationBuilder.DropColumn(
                name: "Fax",
                table: "Distributors");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Distributors");

            migrationBuilder.DropColumn(
                name: "LegalName",
                table: "Distributors");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Distributors");
        }
    }
}
