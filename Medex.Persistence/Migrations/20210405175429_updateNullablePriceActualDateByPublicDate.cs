﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Medex.Persistence.Migrations
{
    public partial class updateNullablePriceActualDateByPublicDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(sql: @"Update PriceItems
SET ActualPriceDate = (Select PublicDate from Prices Where Prices.Id = PriceItems.PriceId) WHERE ActualPriceDate IS NULL",
true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
