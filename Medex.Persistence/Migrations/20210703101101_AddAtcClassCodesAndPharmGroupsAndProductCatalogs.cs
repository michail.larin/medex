﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Medex.Persistence.Migrations
{
    public partial class AddAtcClassCodesAndPharmGroupsAndProductCatalogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "AtcClassCodeId",
                table: "Products",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "PharmGroupId",
                table: "Products",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "AtcClassCodes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AtcClassCodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PharmGroups",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PharmGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductCatalogs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DocumentId = table.Column<long>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false, defaultValueSql: "GETDATE()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCatalogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductCatalogs_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_AtcClassCodeId",
                table: "Products",
                column: "AtcClassCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_PharmGroupId",
                table: "Products",
                column: "PharmGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCatalogs_DocumentId",
                table: "ProductCatalogs",
                column: "DocumentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_AtcClassCodes_AtcClassCodeId",
                table: "Products",
                column: "AtcClassCodeId",
                principalTable: "AtcClassCodes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_PharmGroups_PharmGroupId",
                table: "Products",
                column: "PharmGroupId",
                principalTable: "PharmGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_AtcClassCodes_AtcClassCodeId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_PharmGroups_PharmGroupId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "AtcClassCodes");

            migrationBuilder.DropTable(
                name: "PharmGroups");

            migrationBuilder.DropTable(
                name: "ProductCatalogs");

            migrationBuilder.DropIndex(
                name: "IX_Products_AtcClassCodeId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_PharmGroupId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "AtcClassCodeId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "PharmGroupId",
                table: "Products");
        }
    }
}
