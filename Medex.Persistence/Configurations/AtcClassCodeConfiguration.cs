﻿using Medex.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Persistence.Configurations
{
    internal class PharmGroupCodeConfiguration : BaseConfiguration<AtcClassCode>, IEntityTypeConfiguration<AtcClassCode>
    {
        public override void Configure(EntityTypeBuilder<AtcClassCode> builder)
        {
            builder.ToTable("AtcClassCodes");
            base.Configure(builder);
            builder.Property(x => x.Name).HasMaxLength(512);
            builder.HasMany(x => x.Products)
                .WithOne(x => x.AtcClassCode)
                .HasForeignKey(x => x.AtcClassCodeId);
        }
    }
}
