﻿using Medex.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Persistence.Configurations
{
    internal class DistributorPriceItemConfiguration : BaseConfiguration<DistributorPriceItem>, IEntityTypeConfiguration<DistributorPriceItem>
    {
        public override void Configure(EntityTypeBuilder<DistributorPriceItem> builder)
        {
            builder.ToTable("DistributorPriceItems");
            base.Configure(builder);
        }
    }
}
