﻿using Medex.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Persistence.Configurations
{
    internal class DistributorPriceConfiguration : BaseConfiguration<DistributorPrice>, IEntityTypeConfiguration<DistributorPrice>
    {
        public override void Configure(EntityTypeBuilder<DistributorPrice> builder)
        {
            builder.ToTable("DistributorPrices");
            builder.HasOne(x => x.Distributor)
                .WithMany(x => x.DistributorPrices)
                .OnDelete(DeleteBehavior.Restrict);
            builder.HasMany(x => x.DistributorPriceItems)
                .WithOne(x => x.DistributorPrice)
                .OnDelete(DeleteBehavior.Cascade);
            base.Configure(builder);
        }
    }
}
