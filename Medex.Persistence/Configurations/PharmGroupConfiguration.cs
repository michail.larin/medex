﻿using Medex.Domains.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Persistence.Configurations
{
    internal class PharmGroupConfiguration : BaseConfiguration<PharmGroup>, IEntityTypeConfiguration<PharmGroup>
    {
        public override void Configure(EntityTypeBuilder<PharmGroup> builder)
        {
            builder.ToTable("PharmGroups");
            base.Configure(builder);
            builder.Property(x => x.Name).HasMaxLength(512);
            builder.HasMany(x => x.Products)
                .WithOne(x => x.PharmGroup)
                .HasForeignKey(x => x.PharmGroupId);
        }
    }
}
