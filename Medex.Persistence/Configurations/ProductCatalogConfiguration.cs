﻿using Medex.Domains.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Persistence.Configurations
{
    internal class ProductCatalogConfiguration : BaseConfiguration<ProductCatalog>, IEntityTypeConfiguration<ProductCatalog>
    {
        public override void Configure(EntityTypeBuilder<ProductCatalog> builder)
        {
            builder.ToTable("ProductCatalogs");
            base.Configure(builder);
            builder.Property(_ => _.CreatedOn).HasDefaultValueSql("GETDATE()");
            builder.Property(_ => _.Status);
            builder.HasOne(_ => _.Document).WithMany()
                .HasForeignKey(_ => _.DocumentId);
        }
    }
}