﻿using Medex.Abstractions.Common;
using Medex.Abstractions.Models;
using Medex.Service.Common;
using Medex.Service.Extensions;
using Medex.Service.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Medex.Site.Extensions
{
    public static class InfrastructureStartupExtension
    {
        public static IServiceCollection AddInfrastructure(
            this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment environment)
        {
            services.Configure<SummaryPriceSettings>(configuration.GetSection("SummaryPriceSettings"));
            services.Configure<EmailSettings>(configuration.GetSection("EmailSettings"));
            services.AddSingleton<IHostingEnvironmentService, HostingEnvironmentService>();
            services.AddApplication();
            return services;
        }
    }
}
