﻿using Medex.Abstractions.Business;
using Medex.Data.Dto;
using Medex.Data.Dto.Base.Paging;
using Medex.Data.Dto.Filtering;
using Medex.Site.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Medex.Site.Controllers.V1
{
    [Route("api/v{version:apiVersion}/productCatalogs")]
    public class ProductCatalogController : BaseController
    {
        readonly IProductCatalogService _productCatalogService;
        public ProductCatalogController(IProductCatalogService productCatalogService)
        {
            _productCatalogService = productCatalogService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseWrapper<ProductCatalogDto>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<ProductCatalogDto>>> Create(ProductCatalogDto data) =>
           Ok(await _productCatalogService.CreateAsync(data));

    }
}
