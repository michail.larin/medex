﻿using Medex.Abstractions.Business;
using Medex.Data.Dto;
using Medex.Data.Dto.Base.Paging;
using Medex.Data.Dto.Filtering;
using Medex.Site.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Medex.Site.Controllers.V1
{
    [Route("api/v{version:apiVersion}/summaryPriceItems")]
    public class SummaryPriceItemController : BaseController
    {
        private readonly ISummaryPriceService _summaryPriceService;

        public SummaryPriceItemController(ISummaryPriceService summaryPriceService)
        {
            _summaryPriceService = summaryPriceService;
        }

        [HttpPost("page")]
        [ProducesResponseType(typeof(ResponseWrapper<PageWrapper<SummaryPriceItemDto>>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<PageWrapper<SummaryPriceItemDto>>>> Page(PageContext<SummaryPriceItemFilter> pageContext) =>
             Ok(await _summaryPriceService.GetPageAsync(pageContext));
        
        
        [HttpGet("dictionaries")]
        [ProducesResponseType(typeof(ResponseWrapper<SummaryPriceDictionaryDto>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<SummaryPriceDictionaryDto>>> GetDictionaries() =>
            Ok(await _summaryPriceService.GetAllDictionariesAsync());

        [HttpGet("all")]
        [ProducesResponseType(typeof(ResponseWrapper<PageWrapper<SummaryPriceItemDto>>), StatusCodes.Status200OK)]
        public ActionResult<ResponseWrapper<SummaryPriceItemDto[]>> GetAll() =>
            Ok(_summaryPriceService.GetAll());

    }
}
