using System.Threading;
using System.Threading.Tasks;
using Medex.Abstractions.Business;
using Medex.Data.Dto;
using Medex.Site.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Medex.Site.Controllers.V1
{
    [Route("api/v{version:apiVersion}/[controller]")]
    public class OrganizationController : BaseController
    {
        readonly IOrganizationService _organizationService;

        public OrganizationController(IOrganizationService productService)
        {
            _organizationService = productService;
        }

        [AllowAnonymous]
        [ProducesResponseType(typeof(ResponseWrapper<string[]>), 200)]
        [HttpGet]
        public async Task<IActionResult> GetAll(CancellationToken cancellationToken)
        {
            var data = await _organizationService.GetAllOrganizationsAsync(cancellationToken);
            return Ok(data);
        }
    }
}
