﻿using Medex.Abstractions.Business;
using Medex.Data.Dto;
using Medex.Site.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Medex.Site.Controllers.V1
{
    [Route("api/v{version:apiVersion}/pharmGroups")]
    public class PharmGroupController : BaseController
    {
        readonly IPharmGroupService _pharmGroupService;
        public PharmGroupController(IPharmGroupService pharmGroupService)
        {
            _pharmGroupService = pharmGroupService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseWrapper<PharmGroupDto>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<PharmGroupDto>>> Create(PharmGroupDto data) =>
           Ok(await _pharmGroupService.CreateAsync(data));

        [HttpPut]
        [ProducesResponseType(typeof(ResponseWrapper<PharmGroupDto>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<PharmGroupDto>>> Update(PharmGroupDto data) =>
          Ok(await _pharmGroupService.UpdateAsync(data));

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResponseWrapper<bool>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<bool>>> Delete(long id) =>
            Ok(await _pharmGroupService.DeleteByIdAsync(id));

        [HttpGet]
        [ProducesResponseType(typeof(ResponseWrapper<ICollection<PharmGroupDto>>), StatusCodes.Status200OK)]
        [ResponseCache(Duration = 288000)]
        public async Task<ActionResult<ResponseWrapper<ICollection<PharmGroupDto>>>> GetAll() =>
         Ok(await _pharmGroupService.GetAllAsync());
    }
}
