﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Medex.Abstractions.Business;
using Medex.Data.Dto;
using Medex.Site.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Medex.Site.Controllers.V1
{
    [Route("api/v{version:apiVersion}/distributorPrices")]

    public class DistributorPriceController: BaseController
    {
        private readonly IDistributorPriceService _distributorPriceService;
        public DistributorPriceController(IDistributorPriceService distributorPriceService)
        {
            _distributorPriceService = distributorPriceService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseWrapper<DistributorPriceDto>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<DistributorPriceDto>>> Create(DistributorPriceDto data) =>
            Ok(await _distributorPriceService.CreateAsync(data));

        [HttpPut]
        [ProducesResponseType(typeof(ResponseWrapper<DistributorPriceDto>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<DistributorPriceDto>>> Update(DistributorPriceDto data) =>
            Ok(await _distributorPriceService.UpdateAsync(data));

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResponseWrapper<bool>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<bool>>> Delete(long id) =>
            Ok(await _distributorPriceService.RemovePriceAsync(id));

        [HttpGet]
        [ProducesResponseType(typeof(ResponseWrapper<ICollection<DistributorPriceDto>>), StatusCodes.Status200OK)]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<DistributorPriceDto>>> GetAll() =>
            Ok(await _distributorPriceService.GetAllAsync());
    }
}
