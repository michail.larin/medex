﻿using Medex.Abstractions.Business;
using Medex.Data.Dto;
using Medex.Site.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Medex.Site.Controllers.V1
{
    [Route("api/v{version:apiVersion}/atcClassCodes")]
    public class AtcClassCodeController : BaseController
    {
        readonly IAtcClassCodeService _atcClassCodeService;
        public AtcClassCodeController(IAtcClassCodeService atcClassCodeService)
        {
            _atcClassCodeService = atcClassCodeService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ResponseWrapper<AtcClassCodeDto>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<AtcClassCodeDto>>> Create(AtcClassCodeDto data) =>
           Ok(await _atcClassCodeService.CreateAsync(data));

        [HttpPut]
        [ProducesResponseType(typeof(ResponseWrapper<AtcClassCodeDto>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<AtcClassCodeDto>>> Update(AtcClassCodeDto data) =>
          Ok(await _atcClassCodeService.UpdateAsync(data));

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResponseWrapper<bool>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ResponseWrapper<bool>>> Delete(long id) =>
            Ok(await _atcClassCodeService.DeleteByIdAsync(id));

        [HttpGet]
        [ProducesResponseType(typeof(ResponseWrapper<ICollection<AtcClassCodeDto>>), StatusCodes.Status200OK)]
        [ResponseCache(Duration = 288000)]
        public async Task<ActionResult<ResponseWrapper<ICollection<AtcClassCodeDto>>>> GetAll()
        {
            return Ok(await _atcClassCodeService.GetAllAsync());
        }
    }
}
