using Medex.Abstractions.Business;
using Medex.Abstractions.Common;
using Medex.Data.Dto.Filtering;
using Medex.Domains.Models;
using Medex.Service.Business;
using Medex.Service.Common.PageQueryProvider;
using Microsoft.Extensions.DependencyInjection;

namespace Medex.Service.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddTransient<IPageQueryProvider<Product, NameFilter>, ProductQueryProvider>();
            services.AddTransient<IPageQueryProvider<Price, PriceFilter>, PriceQueryProvider>();
            services.AddTransient<IPageQueryProvider<Distributor, DistributorFilter>, DistributorQueryProvider>(
            );
            services.AddTransient<IPageQueryProvider<User, UserFilter>, UserQueryProvider>();
            services.AddTransient<IPageQueryProvider<PriceItem, PriceItemFilter>, PriceItemQueryProvider>();

            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IPriceItemService, PriceItemService>();
            services.AddTransient<IPriceService, PriceService>();
            services.AddTransient<IInterNameService, InterNameService>();
            services.AddTransient<IDocumentService, DocumentService>();
            services.AddTransient<IDistributorService, DistributorService>();
            services.AddTransient<IManufacturerService, ManufactureService>();
            services.AddTransient<IGroupNameService, GroupNameService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IPriceLoaderService, PriceLoaderService>();
            services.AddTransient<IEmailTemplateFactory, EmailTemplateFactory>();
            services.AddTransient<IAtcClassCodeService, AtcClassCodeService>();
            services.AddTransient<IPharmGroupService, PharmGroupService>();
            services.AddTransient<IProductCatalogService, ProductCatalogService>();
            services.AddTransient<IOrganizationService, OrganizationService>();
            services.AddTransient<IProductCatalogLoaderService, ProductCatalogLoaderService>();
            services.AddSingleton<ISummaryPriceAccessor, SummaryPriceAccessor>();
            services.AddTransient<IDistributorPriceService, DistributorPriceService>();
            services
                .AddTransient<IPageQueryProvider<SummaryPriceItem, SummaryPriceItemFilter>,
                    SummaryPriceItemQueryProvider>();
            services.AddTransient<ISummaryPriceService, SummaryPriceService>();

            return services;
        }
    }
}
