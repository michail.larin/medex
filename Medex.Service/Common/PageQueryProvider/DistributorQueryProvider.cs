﻿using Medex.Abstractions.Common;
using Medex.Data.Dto.Base.Sorting;
using Medex.Data.Dto.Filtering;
using Medex.Data.Primitives;
using Medex.Domains.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Medex.Service.Common.PageQueryProvider
{
    public class DistributorQueryProvider : IPageQueryProvider<Distributor, DistributorFilter>
    {
        public IQueryable<Distributor> Filter(IQueryable<Distributor> query, DistributorFilter filter)
        {
            if (!string.IsNullOrEmpty(filter?.Name?.Value))
            {
                query = query.Where(x => EF.Functions.Like(x.Name, $"%{filter.Name.Value}%"));
            }

            if (!string.IsNullOrEmpty(filter?.Address?.Value))
            {
                query = query.Where(x => x.Address.Contains(filter.Address.Value));
            }

            if (!string.IsNullOrEmpty(filter?.Details?.Value))
            {
                query = query.Where(x => x.Details.Contains(filter.Details.Value));
            }

            if (!string.IsNullOrEmpty(filter?.Email?.Value))
            {
                query = query.Where(x => x.Email.Contains(filter.Email.Value));
            }

            if (!string.IsNullOrEmpty(filter?.Fax?.Value))
            {
                query = query.Where(x => x.Fax.Contains(filter.Fax.Value));
            }

            if (!string.IsNullOrEmpty(filter?.Phone?.Value))
            {
                query = query.Where(x => x.Phone.Contains(filter.Phone.Value));
            }

            if (!string.IsNullOrEmpty(filter?.LegalName?.Value))
            {
                query = query.Where(x => x.Phone.Contains(filter.LegalName.Value));
            }

            return query;
        }

        public IQueryable<Distributor> Sort(IQueryable<Distributor> query, IEnumerable<SorterDescriptor> sorters)
        {
            if (sorters.Any())
            {
                foreach (var sorter in sorters)
                {
                    if (sorter.Field == "name")
                    {
                        if (sorter.Direction == EnumSortDirection.Desceding)
                        {
                            query = query.OrderByDescending(x => x.Name);
                        }
                        else
                        {
                            query = query.OrderBy(x => x.Name);
                        }
                    }

                    if (sorter.Field == "legalName")
                    {
                        if (sorter.Direction == EnumSortDirection.Desceding)
                        {
                            query = query.OrderByDescending(x => x.LegalName);
                        }
                        else
                        {
                            query = query.OrderBy(x => x.LegalName);
                        }
                    }

                    if (sorter.Field == "address")
                    {
                        if (sorter.Direction == EnumSortDirection.Desceding)
                        {
                            query = query.OrderByDescending(x => x.Address);
                        }
                        else
                        {
                            query = query.OrderBy(x => x.Address);
                        }
                    }

                    if (sorter.Field == "email")
                    {
                        if (sorter.Direction == EnumSortDirection.Desceding)
                        {
                            query = query.OrderByDescending(x => x.Email);
                        }
                        else
                        {
                            query = query.OrderBy(x => x.Email);
                        }
                    }

                    if (sorter.Field == "phone")
                    {
                        if (sorter.Direction == EnumSortDirection.Desceding)
                        {
                            query = query.OrderByDescending(x => x.Phone);
                        }
                        else
                        {
                            query = query.OrderBy(x => x.Phone);
                        }
                    }

                    if (sorter.Field == "fax")
                    {
                        if (sorter.Direction == EnumSortDirection.Desceding)
                        {
                            query = query.OrderByDescending(x => x.Fax);
                        }
                        else
                        {
                            query = query.OrderBy(x => x.Fax);
                        }
                    }
                    if (sorter.Field == "details")
                    {
                        if (sorter.Direction == EnumSortDirection.Desceding)
                        {
                            query = query.OrderByDescending(x => x.Details);
                        }
                        else
                        {
                            query = query.OrderBy(x => x.Details);
                        }
                    }
                }
            }
            else
            {
                query = query.OrderBy(x => x.Name);
            }

            return query;
        }
    }
}
