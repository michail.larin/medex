﻿using Medex.Abstractions.Common;
using Medex.Data.Dto.Base.Sorting;
using Medex.Data.Dto.Filtering;
using Medex.Domains.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Medex.Service.Common.PageQueryProvider
{
    public class SummaryPriceItemQueryProvider : IPageQueryProvider<SummaryPriceItem, SummaryPriceItemFilter>
    {
        public IQueryable<SummaryPriceItem> Filter(IQueryable<SummaryPriceItem> query, SummaryPriceItemFilter filter)
        {
            if (filter != null)
            {
                if (filter.ProductName is {Value: { }})
                {
                    query = query.Where(x => x.ProductName.Contains(filter.ProductName.Value));
                }

                if (filter.ProductName != null && filter.ProductName.Values?.Count > 0)
                {
                    query = query.Where(x => filter.ProductName.Values.Contains(x.ProductName));
                }

                if (filter.Country?.Value != null)
                {
                    query = query.Where(x => x.Country.Contains(filter.Country.Value));
                }

                if (filter.Distributor?.Value != null)
                {
                    query = query.Where(x => x.Distributor.Contains(filter.Distributor.Value));
                }

                if (filter.Distributor?.Values?.Count > 0)
                {
                    query = query.Where(x => filter.Distributor.Values.Contains(x.Distributor));
                }

                if (filter.Manufacturer?.Value != null)
                {
                    query = query.Where(x => x.Manufacturer.Contains(filter.Manufacturer.Value));
                }

                if (filter.Manufacturer?.Values?.Count > 0)
                {
                    query = query.Where(x => filter.Manufacturer.Values.Contains(x.Manufacturer));
                }

                if (filter.Country?.Values?.Count > 0)
                {
                    query = query.Where(x => filter.Country.Values.Contains(x.Country));
                }

                if (filter.GroupName?.Value != null)
                {
                    query = query.Where(x => x.GroupName.Contains(filter.GroupName.Value));
                }

                if (filter.GroupName?.Values?.Count > 0)
                {
                    query = query.Where(x => filter.GroupName.Values.Contains(x.GroupName));
                }

                if (filter.PharmGroup != null && filter.PharmGroup.Value != null)
                {
                    query = query.Where(x => x.PharmGroup.Contains(filter.PharmGroup.Value));
                }

                if (filter.PharmGroup?.Values?.Count > 0)
                {
                    query = query.Where(x => filter.PharmGroup.Values.Contains(x.PharmGroup));
                }

                if (filter.AtcClassCode != null && filter.AtcClassCode.Value != null)
                {
                    query = query.Where(x => x.AtcClassCode.Contains(filter.AtcClassCode.Value));
                }

                if (filter.AtcClassCode?.Values?.Count > 0)
                {
                    query = query.Where(x => filter.AtcClassCode.Values.Contains(x.AtcClassCode));
                }

                if (filter.InterName != null && filter.InterName.Value != null)
                {
                    query = query.Where(x => x.InterName.Contains(filter.InterName.Value));
                }

                if (filter.InterName?.Values?.Count > 0)
                {
                    query = query.Where(x => filter.InterName.Values.Contains(x.InterName));
                }

                // public date
                if (filter?.PublicDate?.Range?.Lte != null)
                {
                    query = query.Where(x => x.PublicDate <= filter.PublicDate.Range.Lte);
                }

                if (filter?.PublicDate?.Range?.Gte != null)
                {
                    query = query.Where(x => x.PublicDate >= filter.PublicDate.Range.Gte);
                }

                if (filter?.PublicDate?.Range?.Lte != null)
                {
                    query = query.Where(x => x.PublicDate <= filter.PublicDate.Range.Lte);
                }

                if (filter?.PublicDate?.Range?.Gte != null)
                {
                    query = query.Where(x => x.PublicDate >= filter.PublicDate.Range.Gte);
                }
                if (filter.PublicDate != null && filter.PublicDate.Range?.Lt != null)
                {
                    query = query.Where(x => x.PublicDate <= filter.PublicDate.Range.Lt);
                }

                if (filter.PublicDate != null && filter.PublicDate.Range?.Gt != null)
                {
                    query = query.Where(x => x.PublicDate >= filter.PublicDate.Range.Gt);
                }
                
                // expired date
                if (filter?.ExpiredDate?.Range?.Lte != null)
                {
                    query = query.Where(x => x.ExpiredDate <= filter.ExpiredDate.Range.Lte);
                }

                if (filter?.ExpiredDate?.Range?.Gte != null)
                {
                    query = query.Where(x => x.ExpiredDate >= filter.ExpiredDate.Range.Gte);
                }

                if (filter?.ExpiredDate?.Range?.Lte != null)
                {
                    query = query.Where(x => x.ExpiredDate <= filter.ExpiredDate.Range.Lte);
                }

                if (filter?.ExpiredDate?.Range?.Gte != null)
                {
                    query = query.Where(x => x.ExpiredDate >= filter.ExpiredDate.Range.Gte);
                }
                if (filter.ExpiredDate != null && filter.ExpiredDate.Range?.Lt != null)
                {
                    query = query.Where(x => x.ExpiredDate <= filter.ExpiredDate.Range.Lt);
                }

                if (filter.ExpiredDate != null && filter.ExpiredDate.Range?.Gt != null)
                {
                    query = query.Where(x => x.ExpiredDate >= filter.ExpiredDate.Range.Gt);
                }
                // cost
                if (filter?.Cost?.Range?.Gte != null)
                {
                    query = query.Where(x => x.Cost >= filter.Cost.Range.Gte);
                }

                if (filter?.Cost?.Range?.Lte != null)
                {
                    query = query.Where(x => x.Cost <= filter.Cost.Range.Lte);
                }

                if (filter?.CostInDollar?.Range?.Gte != null)
                {
                    query = query.Where(x => x.CostInDollar >= filter.CostInDollar.Range.Gte);
                }

                if (filter?.CostInDollar?.Range?.Lte != null)
                {
                    query = query.Where(x => x.CostInDollar <= filter.CostInDollar.Range.Lte);
                }

                if (filter?.CostInEuro?.Range?.Gte != null)
                {
                    query = query.Where(x => x.CostInEuro >= filter.CostInEuro.Range.Gte);
                }

                if (filter?.CostInEuro?.Range?.Lte != null)
                {
                    query = query.Where(x => x.CostInEuro <= filter.CostInEuro.Range.Lte);
                }
            }

            return query;
        }

        public IQueryable<SummaryPriceItem> Sort(IQueryable<SummaryPriceItem> query,
            IEnumerable<SorterDescriptor> sorters)
        {
            if (sorters.Any())
            {
                foreach (var sorter in sorters)
                {
                    if (sorter.Field == "cost")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.Cost)
                            : query.OrderBy(x => x.Cost);
                    }
                    
                    if (sorter.Field == "expiredDate")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.ExpiredDate)
                            : query.OrderBy(x => x.ExpiredDate);
                    }

                    if (sorter.Field == "costInDollar")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.CostInDollar)
                            : query.OrderBy(x => x.CostInDollar);
                    }

                    if (sorter.Field == "costInEuro")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.CostInEuro)
                            : query.OrderBy(x => x.CostInEuro);
                    }

                    if (sorter.Field == "createdOn")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.CreatedOn)
                            : query.OrderBy(x => x.CreatedOn);
                    }

                    if (sorter.Field == "publicDate")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.PublicDate)
                            : query.OrderBy(x => x.PublicDate);
                    }


                    if (sorter.Field == "distributor")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.Distributor)
                            : query.OrderBy(x => x.Distributor);
                    }

                    if (sorter.Field == "margin")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.Margin)
                            : query.OrderBy(x => x.Margin);
                    }

                    if (sorter.Field == "productName")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.ProductName)
                            : query.OrderBy(x => x.ProductName);
                    }

                    if (sorter.Field == "manufacturer")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.Manufacturer)
                            : query.OrderBy(x => x.Manufacturer);
                    }

                    if (sorter.Field == "groupName")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.GroupName)
                            : query.OrderBy(x => x.GroupName);
                    }

                    if (sorter.Field == "pharmGroup")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.PharmGroup)
                            : query.OrderBy(x => x.PharmGroup);
                    }

                    if (sorter.Field == "atcClassCode")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.AtcClassCode)
                            : query.OrderBy(x => x.AtcClassCode);
                    }

                    if (sorter.Field == "interName")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.InterName)
                            : query.OrderBy(x => x.InterName);
                    }

                    if (sorter.Field == "country")
                    {
                        query = sorter.Direction == Data.Primitives.EnumSortDirection.Desceding
                            ? query.OrderByDescending(x => x.Country)
                            : query.OrderBy(x => x.Country);
                    }
                }
            }
            else
            {
                query = query.OrderBy(x => x.ProductName);
            }
            return query;
        }
    }
}
