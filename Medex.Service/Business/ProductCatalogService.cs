﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Medex.Abstractions.Business;
using Medex.Abstractions.Common;
using Medex.Abstractions.Persistence;
using Medex.Data.Dto;
using Medex.Data.Dto.Filtering;
using Medex.Data.Primitives;
using Medex.Domains.Models;
using Medex.Service.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Medex.Service.Business
{
    public class ProductCatalogService : BaseRestService<ProductCatalog, ProductCatalogDto, NameFilter>, IProductCatalogService
    {
        public ProductCatalogService(
            IApplicationDbContext dbContext,
            IMapper mapper) : base(dbContext, mapper)
        {
        }

        public override Task<ProductCatalogDto> CreateAsync(ProductCatalogDto dto)
        {
            dto.Status = EnumProductCatalogStatusCodes.New;
            return base.CreateAsync(dto);
        }

        public async Task<ProductCatalogDto[]> GetAllNewProductCatalogsAsync()
        {
            var result = await _dbContext.ProductCatalogs
                .Where(x => x.Status == (int)EnumProductCatalogStatusCodes.New)
                .ProjectTo<ProductCatalogDto>(_mapper.ConfigurationProvider)
                .ToArrayAsync();
            return result;
        }
    }
}
