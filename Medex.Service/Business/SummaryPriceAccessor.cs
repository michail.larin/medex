﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Medex.Abstractions.Business;
using Medex.Abstractions.Models;
using Medex.Domains.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Medex.Service.Business
{
    public class SummaryPriceAccessor : ISummaryPriceAccessor
    {
        private readonly IOptions<SummaryPriceSettings> _summaryPriceSettings;
        private List<SummaryPriceItem> _summaryPriceItems = new List<SummaryPriceItem>();
        private readonly object _syncObj = new object();

        private SummaryPriceDictionary _dictionaries;

        public SummaryPriceAccessor(IOptions<SummaryPriceSettings> summaryPriceSettings)
        {
            _summaryPriceSettings = summaryPriceSettings;
            ReadSummaryPriceList();
        }

        private void ReadSummaryPriceList()
        {
            lock (_syncObj)
            {
                if (File.Exists(_summaryPriceSettings.Value.Path))
                {
                    var serializer = new JsonSerializer();
                    using var fs = new FileStream(_summaryPriceSettings.Value.Path, FileMode.Open);
                    using var sr = new StreamReader(fs);
                    using var jsonTextReader = new JsonTextReader(sr);
                    _summaryPriceItems = serializer.Deserialize<List<SummaryPriceItem>>(jsonTextReader);
                    ReloadDictionaries();
                }
                else
                {
                    _summaryPriceItems = new List<SummaryPriceItem>();
                }
            }
        }

        private void ReloadDictionaries()
        {
            _dictionaries = new SummaryPriceDictionary
            {
                Products = _summaryPriceItems.Select(x => x.ProductName).Distinct().OrderBy(x => x).ToArray(),
                Distributors = _summaryPriceItems.Select(x => x.Distributor).Distinct().OrderBy(x => x).ToArray(),
                Manufacturers = _summaryPriceItems.Select(x => x.Manufacturer).Distinct().OrderBy(x => x).ToArray(),
                GroupNames = _summaryPriceItems.Select(x => x.GroupName).Distinct().OrderBy(x => x).ToArray(),
                Countries = _summaryPriceItems.Select(x => x.Country).Distinct().OrderBy(x => x).ToArray(),
                InterNames = _summaryPriceItems.Select(x => x.InterName).Distinct().OrderBy(x => x).ToArray(),
                PharmGroups = _summaryPriceItems.Select(x => x.PharmGroup).Distinct().OrderBy(x => x).ToArray(),
                AtcClassCodes = _summaryPriceItems.Select(x => x.AtcClassCode).Distinct().OrderBy(x => x).ToArray(),
            };
        }

        private void SaveSummaryPriceList()
        {
            var serializer = new JsonSerializer();
            if (File.Exists(_summaryPriceSettings.Value.Path))
            {
                File.Delete(_summaryPriceSettings.Value.Path);
            }

            using var fs = new FileStream(_summaryPriceSettings.Value.Path, FileMode.OpenOrCreate,
                FileAccess.Write);
            using var sw = new StreamWriter(fs);
            using var jsonTextWriter = new JsonTextWriter(sw);
            serializer.Serialize(jsonTextWriter, _summaryPriceItems);
            ReloadDictionaries();
        }

        public void AddPriceList(IList<SummaryPriceItem> summaryPriceItems, HashSet<long> distributorIds)
        {
            lock (_syncObj)
            {
                if (distributorIds.Count > 1)
                {
                    _summaryPriceItems.Clear();
                }
                else
                {
                    _summaryPriceItems.RemoveAll(x => distributorIds.Contains(x.DistributorId));
                }

                _summaryPriceItems.AddRange(summaryPriceItems);
                SaveSummaryPriceList();
            }
        }

        public void RemoveByDistributorId(long distributorId)
        {
            lock (_syncObj)
            {
                _summaryPriceItems.RemoveAll(x => x.DistributorId == distributorId);
                SaveSummaryPriceList();
            }
        }

        public List<SummaryPriceItem> All => GetAll();

        public SummaryPriceDictionary Dictionaries => _dictionaries;

        private List<SummaryPriceItem> GetAll()
        {
            lock (_syncObj)
            {
                return _summaryPriceItems.GetRange(0, _summaryPriceItems.Count);
            }
        }
    }
}