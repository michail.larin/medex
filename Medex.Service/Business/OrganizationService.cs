using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Medex.Abstractions.Business;
using Medex.Abstractions.Persistence;
using Medex.Service.Common;
using Microsoft.EntityFrameworkCore;

namespace Medex.Service.Business
{
    public class OrganizationService : BaseService, IOrganizationService
    {
        public OrganizationService(IApplicationDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }

        public Task<string[]> GetAllOrganizationsAsync(CancellationToken cancellationToken)
        {
            return _dbContext.Users
                .Where(x => x.Organization != null)
                .Select(x => x.Organization).Distinct()
                .ToArrayAsync(cancellationToken);
        }
    }
}
