﻿using AutoMapper;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Medex.Abstractions.Persistence;
using Medex.Data.Primitives;
using Medex.Domains.Models;
using Medex.Service.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Medex.Abstractions.Business;

namespace Medex.Service.Business
{
    public class PriceLoaderService : BaseService, IPriceLoaderService
    {
        public PriceLoaderService(IApplicationDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {

        }

        private async Task<IList<(Product product, Distributor distributor, PriceItem priceItem)>> LoadProductsItemsAsync(Stream stream, Price price)
        {
            var products = await _dbContext.Products.Include(x => x.GroupName)
                .Include(x => x.Manufacture)
                .Include(x => x.AtcClassCode)
                .Include(x => x.PharmGroup)
                .Include(x => x.InterName).ToListAsync();
            var distributors = await _dbContext.Distributors.ToListAsync();
            var manufacturers = products.Select(x => x.Manufacture).Distinct().ToList();
            var interNames = products.Select(x => x.InterName).Distinct().ToList();
            var groupNames = products.Select(x => x.GroupName).Distinct().ToList();

            var emptyManufacturer = products.Select(x => x.Manufacture).FirstOrDefault(x => x.Name == string.Empty);
            var emptyAtcClassCode = products.Select(x => x.AtcClassCode).FirstOrDefault(x => x.Name == string.Empty);
            var emptyPharmGroup = products.Select(x => x.PharmGroup).FirstOrDefault(x => x.Name == string.Empty);
            var emptyGroup = products.Select(x => x.GroupName).FirstOrDefault(x => x.Name == string.Empty);
            var emptyInterName = products.Select(x => x.InterName).FirstOrDefault(x => x.Name == string.Empty);

            var decimalSeparator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var results = new List<(Product product, Distributor distributor, PriceItem priceItem)>();
            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(stream, false))
            {
                WorkbookPart workbookPart = doc.WorkbookPart;
                Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
                Sheet sheet = (Sheet)sheets.First();
                Worksheet workSheet = ((WorksheetPart)workbookPart.GetPartById(sheet.Id)).Worksheet;

                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                foreach (var openXmlElement in sheetData)
                {
                    var row = (Row) openXmlElement;
                    var cells = new List<string>();

                    try
                    {
                        foreach (var openXmlElement1 in row)
                        {
                            var cell = (Cell) openXmlElement1;
                            if (cell.DataType != null)
                            {
                                if (cell.DataType == CellValues.SharedString)
                                {
                                    if (int.TryParse(cell.InnerText, out var id))
                                    {
                                        SharedStringItem item = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);
                                        cells.Add(item.InnerText);
                                    }
                                    else
                                    {
                                        cells.Add(cell.InnerText);
                                    }
                                }
                                else
                                {
                                    cells.Add(cell.InnerText);
                                }
                            }
                            else
                            {
                                cells.Add(cell.InnerText);
                            }
                        }
                        if (cells.Count < 11)
                        {
                            break;
                        }
                        if (string.IsNullOrEmpty(cells[0]))
                        {
                            break;
                        }
                        string productName = cells[0]?.Trim();
                        string interName = cells[1]?.Trim();
                        string groupName = cells[2]?.Trim();
                        string distributorName = cells[8]?.Trim();
                        string manufacturerName = cells[9]?.Trim();
                        string country = cells[10]?.Trim();
                        string actualPriceDateStr = cells.Count > 11 ? cells[11]?.Trim() : null;


                        decimal margin;
                        margin = string.IsNullOrEmpty(cells[3]) ? 0 : (decimal.TryParse(cells[3].Replace("%", "")
                            .Replace(".", decimalSeparator).Replace(",", decimalSeparator), out margin) ? margin : 0);

                        decimal cost;
                        cost = decimal.TryParse(cells[4].Replace(".", decimalSeparator).Replace(",", decimalSeparator), out cost) ? cost : -1;
                        cost = string.IsNullOrEmpty(cells[4]) || cells[4].StartsWith("догов") ? -1 :
                            (cells[4].StartsWith("ожидаемый") ? -2 : cost);

                        decimal costDollar;
                        costDollar = decimal.TryParse(cells[5].Replace(".", decimalSeparator).Replace(",", decimalSeparator), out costDollar) ? costDollar : -1;
                        costDollar = string.IsNullOrEmpty(cells[5]) || cells[5].StartsWith("догов") ? -1 :
                            (cells[5].StartsWith("ожидаемый") ? -2 : costDollar);

                        decimal costEuro;
                        costEuro = decimal.TryParse(cells[6].Replace(".", decimalSeparator).Replace(",", decimalSeparator), out costEuro) ? costEuro : -1;
                        costEuro = string.IsNullOrEmpty(cells[6]) || cells[6].StartsWith("догов") ? -1 :
                            (cells[6].StartsWith("ожидаемый") ? -2 : costEuro);

                        var priceItem = new PriceItem
                        {
                            PriceId = price.Id,
                            Cost = cost,
                            CostInDollar = costDollar,
                            CostInEuro = costEuro,
                            Margin = margin,
                        };

                        if (DateTime.TryParse(actualPriceDateStr, out DateTime actualPriceDate))
                        {
                            priceItem.ActualPriceDate = actualPriceDate;
                        }
                        else
                        {
                            priceItem.ActualPriceDate = double.TryParse(actualPriceDateStr, out double dateNum) ? DateTime.FromOADate(dateNum) : price.PublicDate;
                        }

                        Manufacturer manufacturer =
                                manufacturers.FirstOrDefault(x => x.Country == country && x.Name == manufacturerName);
                        if (manufacturer == null)
                        {
                            if (string.IsNullOrEmpty(manufacturerName))
                            {
                                manufacturer = emptyManufacturer;
                            }
                            else
                            {
                                manufacturer = new Manufacturer
                                {
                                    Country = country,
                                    Name = manufacturerName
                                };
                                _dbContext.Manufacturers.Add(manufacturer);
                                manufacturers.Add(manufacturer);
                            }
                        }


                        Distributor distributor = distributors.FirstOrDefault(x => x.Name == distributorName);

                        if (distributor == null)
                        {
                            distributor = new Distributor
                            {
                                Name = distributorName,
                                IsActive = true,
                            };
                            distributors.Add(distributor);
                            _dbContext.Distributors.Add(distributor);

                        }
                        InterName inter = interNames.FirstOrDefault(x => x.Name == interName);
                        if (inter == null)
                        {
                            if (string.IsNullOrEmpty(interName))
                            {
                                inter = emptyInterName;
                            }
                            else
                            {
                                inter = new InterName
                                {
                                    Name = interName,
                                };
                                interNames.Add(inter);
                                _dbContext.InterNames.Add(inter);
                            }
                        }

                        GroupName group =
                                groupNames.FirstOrDefault(x => x.Name == groupName);

                        if (group == null)
                        {
                            if (string.IsNullOrEmpty(groupName))
                            {
                                group = emptyGroup;
                            }
                            else
                            {
                                group = new GroupName
                                {
                                    Name = groupName,
                                };
                                groupNames.Add(group);
                                _dbContext.GroupNames.Add(group);
                            }
                        }

                        var productsQuery = products.AsQueryable();
                        productsQuery = productsQuery.Where(x => x.Name == productName);

                        var productWithTheSameName = productsQuery.FirstOrDefault();

                        if (manufacturer != null)
                        {
                            productsQuery = productsQuery.Where(x => x.Manufacture.Name == manufacturer.Name
                        && x.Manufacture.Country == manufacturer.Country);
                        }
                        var product = productsQuery.FirstOrDefault();
                        if (product == null)
                        {
                            product = new Product
                            {
                                Manufacture = manufacturer,
                                Name = productName,
                                AtcClassCode = productWithTheSameName?.AtcClassCode ?? emptyAtcClassCode,
                                PharmGroup = productWithTheSameName?.PharmGroup ?? emptyPharmGroup,
                                InterName = productWithTheSameName?.InterName ?? inter,
                                GroupName = productWithTheSameName?.GroupName ?? group,
                            };
                            products.Add(product);
                            _dbContext.Products.Add(product);
                        }
                        results.Add((product, distributor, priceItem));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
            await _dbContext.SaveChangesAsync();
                
            return results;
        }

        public async Task LoadPriceAsync(long priceId)
        {
            var price = await _dbContext.Prices.FirstOrDefaultAsync(x => x.Id == priceId);
            if ((EnumPriceStatusCode)price.Status == EnumPriceStatusCode.Processing)
            {
                throw new Exception("Price is already in processing");
            }
            if ((EnumPriceStatusCode)price.Status == EnumPriceStatusCode.Active)
            {
                throw new Exception("Price has already been processed");
            }

            price.Status = (int)EnumPriceStatusCode.Processing;
            await _dbContext.SaveChangesAsync();
            var document = await _dbContext.Documents.FirstOrDefaultAsync(x => x.Id == price.DocumentId);

            await using var stream = new MemoryStream(document.Data);
            IList<(Product product, Distributor distributor, PriceItem priceItem)> productProcessResult = await LoadProductsItemsAsync(stream, price);
            var priceItems = new List<PriceItem>();
            foreach (var item in productProcessResult)
            {
                item.priceItem.DistributorId = item.distributor.Id;
                item.priceItem.ProductId = item.product.Id;
                priceItems.Add(item.priceItem);
            }
            price.Status = (int)EnumPriceStatusCode.Active;
            await _dbContext.BulkAddAsync(priceItems);
            await _dbContext.SaveChangesAsync();
        }
    }
}
