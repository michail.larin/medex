﻿using Medex.Abstractions.Business;
using Medex.Abstractions.Models;
using Medex.Abstractions.Primitives;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Service.Business
{
    public class EmailTemplateFactory : IEmailTemplateFactory
    {
        public EmailTemplate Create(EnumEmailTypes emailType, string from, string to, string messageBody, string subject, params object[] parameters)
        {
            switch (emailType)
            {
                case EnumEmailTypes.ForgotPassword:
                    return CreateForgotPassword(from, to, messageBody, subject, parameters);
                default:
                    throw new NotImplementedException($"Email type {emailType} is not implemented");
            }
        }

        private EmailTemplate CreateForgotPassword(string from, string to, string messageBody, string subject, params object[] parameters)
        {
            var template = new EmailTemplate
            {
                MessageBody = string.Format(messageBody, parameters),
                Subject = subject,
                From = from,
                To = {to}
            };
            return template;
        }
    }
}
