﻿using System;
using AutoMapper;
using Medex.Abstractions.Business;
using Medex.Abstractions.Persistence;
using Medex.Data.Dto;
using Medex.Data.Dto.Filtering;
using Medex.Domains.Models;
using Medex.Service.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Medex.Abstractions.Common;
using Medex.Data.Dto.Base.Paging;
using Medex.Service.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Medex.Service.Business
{
    public class DistributorService : BasePageableService<Distributor, DistributorDto, DistributorFilter>,
        IDistributorService
    {
        public DistributorService(IApplicationDbContext dbContext, IMapper mapper,
            IPageQueryProvider<Distributor, DistributorFilter> queryProvider) : base(dbContext, mapper, queryProvider)
        {
        }

        public override async Task<PageWrapper<DistributorDto>> GetWithPaging(PageContext<DistributorFilter> context)
        {
            var query = _dbContext.Distributors.AsQueryable();
            query = query.UsePageContext(_queryProvider, context);
            var (ids, count) = (await query.Select(x => x.Id)
                .Skip(context.Skip).Take(context.Take).ToListAsync(), await query.CountAsync());

            query = _dbContext.Distributors.AsQueryable();

            query = query
                .Where(x => ids.Contains(x.Id));
            query = _queryProvider.Sort(query, context.SortList);
            query = query.AsNoTracking();

            var list = await query.ToListAsync();
            return new PageWrapper<DistributorDto>()
            {
                Offset = context.Skip,
                Count = context.Take,
                TotalCount = count,
                Data = _mapper.Map<IEnumerable<DistributorDto>>(list),
            };
        }

        public override async Task<IList<DistributorDto>> GetAllAsync(DistributorFilter filter = null)
        {
            var result = await base.GetAllAsync(filter);
            return result.OrderBy(x => x.Name).ToList();
        }
    }
}
