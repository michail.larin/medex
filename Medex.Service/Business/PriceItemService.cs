﻿using AutoMapper;
using Medex.Abstractions.Business;
using Medex.Abstractions.Common;
using Medex.Abstractions.Persistence;
using Medex.Data.Dto;
using Medex.Data.Primitives;
using Medex.Data.Dto.Base.Paging;
using Medex.Data.Dto.Filtering;
using Medex.Domains.Models;
using Medex.Service.Common;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Medex.Service.Extensions;
using System.Collections.Generic;

namespace Medex.Service.Business
{
    public class PriceItemService : BasePageableService<PriceItem, PriceItemDto, PriceItemFilter>, IPriceItemService
    {
        public PriceItemService(
            IApplicationDbContext dbContext,
            IMapper mapper,
            IPageQueryProvider<PriceItem, PriceItemFilter> queryProvider) : base(dbContext, mapper, queryProvider)
        {

        }

        public async Task<PageWrapper<PriceItemDto>> GetLastPriceListAsync(PageContext<PriceItemFilter> pageContext)
        {
            var priceList = await this._dbContext.Prices
                .Where(x => x.Status == (int)EnumPriceStatusCode.Active)
                .OrderByDescending(x => x.PublicDate).FirstOrDefaultAsync();

            if (priceList == null)
            {
                return new PageWrapper<PriceItemDto>
                {
                    Count = 0,
                    Offset = 0,
                    TotalCount = 0,
                    Data = new List<PriceItemDto>(),
                };
            }
            pageContext.Filter.PriceId = new Data.Dto.Base.Filtering.FilterDescriptor<long?>
            {
                Value = priceList.Id
            };
            var result = await this.GetWithPaging(pageContext);
            return result;

        }

        public override async Task<PageWrapper<PriceItemDto>> GetWithPaging(PageContext<PriceItemFilter> context)
        {
            var query = _dbContext.PriceItems.AsQueryable();
            query = query.UsePageContext(_queryProvider, context);

            var tm1 = System.Environment.TickCount;
            var (ids, count) = (await query.Select(x => x.Id)
                .Skip(context.Skip).Take(context.Take).ToListAsync(), await query.CountAsync());

            query = _dbContext.PriceItems.AsQueryable();

            query = query.Include(x => x.Product)
            .Include(x => x.Distributor)
            .Include(x => x.Product.Manufacture)
            .Include(x => x.Product.InterName)
            .Include(x => x.Product.GroupName)
            .Include(x => x.Product.AtcClassCode)
            .Include(x => x.Product.PharmGroup)
            .Include(x => x.Price)
            .Where(x => ids.Contains(x.Id));
            query = _queryProvider.Sort(query, context.SortList);
            query = query.AsNoTracking();

            var list = await query.ToListAsync();
            var tm2 = System.Environment.TickCount -tm1;
            return new PageWrapper<PriceItemDto>
            {
                Offset = context.Skip,
                Count = context.Take,
                TotalCount = count,
                Data = _mapper.Map<IEnumerable<PriceItemDto>>(list),
            };
        }
    }
}
