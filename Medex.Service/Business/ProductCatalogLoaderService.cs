﻿using AutoMapper;
using CsvHelper;
using CsvHelper.Configuration;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Medex.Abstractions.Common;
using Medex.Abstractions.Persistence;
using Medex.Data.Primitives;
using Medex.Domains.Models;
using Medex.Service.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Medex.Service.Business
{
    public class ProductCatalogLoaderService : BaseService, IProductCatalogLoaderService
    {
        private readonly IConfiguration _configuration;
        public ProductCatalogLoaderService(IApplicationDbContext dbContext,
            IMapper mapper,
            IConfiguration configuration) : base(dbContext, mapper)
        {
            _configuration = configuration;

        }

        private async Task<IList<Product>> LoadProductsItemsAsync(Stream stream, ProductCatalog productCatalog)
        {
            var products = await _dbContext.Products.Include(x => x.GroupName)
                .Include(x => x.Manufacture)
                .Include(x => x.AtcClassCode)
                .Include(x => x.PharmGroup)
                .Include(x => x.InterName).ToListAsync();
            var distributors = await _dbContext.Distributors.ToListAsync();
            var manufacturers = products.Select(x => x.Manufacture).Distinct().ToList();
            var interNames = products.Select(x => x.InterName).Distinct().ToList();
            var groupNames = products.Select(x => x.GroupName).Distinct().ToList();
            var pharmGroups = products.Select(x => x.PharmGroup).Distinct().ToList();
            var atcClassCodes = products.Select(x => x.AtcClassCode).Distinct().ToList();

            var decimalSeparator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var results = new List<Product>();
            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(stream, false))
            {
                WorkbookPart workbookPart = doc.WorkbookPart;
                Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
                Sheet sheet = (Sheet)sheets.FirstOrDefault();
                Worksheet workSheet = ((WorksheetPart)workbookPart.GetPartById(sheet.Id)).Worksheet;

                SheetData sheetData = (SheetData)workSheet.GetFirstChild<SheetData>();
                var index = 0;
                foreach (Row row in sheetData)
                {
                    var cells = new List<string>();
                    
                    if (row.RowIndex == "1")
                    {
                        continue;                     
                    }

                    try
                    {

                        foreach (Cell cell in row)
                        {
                            if (cell.DataType != null)
                            {
                                if (cell.DataType == CellValues.SharedString)
                                {
                                    int id;
                                    if (int.TryParse(cell.InnerText, out id))
                                    {
                                        SharedStringItem item = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);
                                        cells.Add(item.InnerText);
                                    }
                                    else
                                    {
                                        cells.Add(cell.InnerText);
                                    }
                                }
                                else
                                {
                                    cells.Add(cell.InnerText);
                                }
                            }
                            else
                            {
                                cells.Add(cell.InnerText);
                            }

                        }
                        if (cells.Count < 10)
                        {
                            break;
                        }
                        if (string.IsNullOrEmpty(cells[0]))
                        {
                            break;
                        }
                        string productName = cells[0]?.Trim();
                        string interName = cells[1]?.Trim();
                        string groupName = cells[2]?.Trim();
                        var manufacturerAr = cells[7]?.Split(',', StringSplitOptions.RemoveEmptyEntries);
                        string manufacturerName = "";
                        string country = "";
                        if (manufacturerAr.Length > 0)
                        {
                            manufacturerName = manufacturerAr[0]?.Trim();
                            country = manufacturerAr[0]?.Trim();
                        }
                        if (manufacturerAr.Length == 2)
                        {
                            country = manufacturerAr[1]?.Trim();
                        }
                        string atcClassCodeName = cells[8]?.Trim();
                        string pharmGroupName = cells[9]?.Trim();

                        if (atcClassCodeName?.Length > 512 || pharmGroupName?.Length > 512)
                        {

                        }

                        Manufacturer manufacturer =
                                manufacturers.FirstOrDefault(x => x.Country == country && x.Name == manufacturerName);
                        if (manufacturer == null)
                        {
                            manufacturer = new Manufacturer
                            {
                                Country = country,
                                Name = manufacturerName
                            };
                            _dbContext.Manufacturers.Add(manufacturer);
                            manufacturers.Add(manufacturer);
                        }


                        InterName inter = interNames.FirstOrDefault(x => x.Name == interName);
                        if (inter == null)
                        {
                            inter = new InterName
                            {
                                Name = interName,
                            };
                            interNames.Add(inter);
                            _dbContext.InterNames.Add(inter);
                        }
                       

                        GroupName group =
                                groupNames.FirstOrDefault(x => x.Name == groupName);

                        if (group == null)
                        {
                            group = new GroupName
                            {
                                Name = groupName,
                            };
                            groupNames.Add(group);
                            _dbContext.GroupNames.Add(group);

                        }

                        PharmGroup pharmGroup = 
                                pharmGroups.FirstOrDefault(x => x.Name == pharmGroupName);

                        if (pharmGroup == null)
                        {
                            pharmGroup = new PharmGroup
                            {
                                Name = pharmGroupName,
                            };
                            pharmGroups.Add(pharmGroup);
                            _dbContext.PharmGroups.Add(pharmGroup);
                        }

                        AtcClassCode atcClassCode=
                                atcClassCodes.FirstOrDefault(x => x.Name == atcClassCodeName);

                        if (atcClassCode == null)
                        {
                            atcClassCode = new AtcClassCode
                            {
                                Name = atcClassCodeName,
                            };
                            atcClassCodes.Add(atcClassCode);
                            _dbContext.AtcClassCodes.Add(atcClassCode);
                        }

                        var productsQuery = products.AsQueryable();
                        productsQuery = productsQuery.Where(x => x.Name == productName);

                        if (manufacturer != null)
                        {
                            productsQuery = productsQuery.Where(x => x.Manufacture.Name == manufacturer.Name
                        && x.Manufacture.Country == manufacturer.Country);
                        }
                        var product = productsQuery.FirstOrDefault();

                        if (product == null)
                        {
                            product = new Product
                            {
                                InterName = inter,
                                GroupName = group,
                                Manufacture = manufacturer,
                                Name = productName,
                                AtcClassCode = atcClassCode,
                                PharmGroup = pharmGroup,
                            };
                            products.Add(product);
                            _dbContext.Products.Add(product);
                        }
                        results.Add(product);
                    }
                    catch (Exception ex)
                    {
                    }
                    index++;
                }
            }
            await _dbContext.SaveChangesAsync();

            return results;
        }

        public async Task LoadProductCatalogAsync(long productCatalogId)
        {
            var productCatalog = await _dbContext.ProductCatalogs.FirstOrDefaultAsync(x => x.Id == productCatalogId);
            if ((EnumProductCatalogStatusCodes)productCatalog.Status == EnumProductCatalogStatusCodes.Processing)
            {
                throw new Exception("product catalog is already in processing");
            }

            productCatalog.Status = (int)EnumProductCatalogStatusCodes.Processing;
            await _dbContext.SaveChangesAsync();
            var document = await _dbContext.Documents.FirstOrDefaultAsync(x => x.Id == productCatalog.DocumentId);

            using (var stream = new MemoryStream(document.Data))
            {

                IList<Product> productProcessResult =
                    await LoadProductsItemsAsync(stream, productCatalog);
                productCatalog.Status = (int)EnumProductCatalogStatusCodes.Completed;
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
