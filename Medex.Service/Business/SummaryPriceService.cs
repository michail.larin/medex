﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Medex.Abstractions.Business;
using Medex.Abstractions.Common;
using Medex.Data.Dto;
using Medex.Data.Dto.Base.Paging;
using Medex.Data.Dto.Filtering;
using Medex.Domains.Models;

namespace Medex.Service.Business
{
    public class SummaryPriceService : ISummaryPriceService
    {
        private readonly IMapper _mapper;
        private readonly ISummaryPriceAccessor _summaryPriceAccessor;
        private readonly IPageQueryProvider<SummaryPriceItem, SummaryPriceItemFilter> _pageQueryProvider;

        public SummaryPriceService(IMapper mapper,
            ISummaryPriceAccessor summaryPriceAccessor,
            IPageQueryProvider<SummaryPriceItem, SummaryPriceItemFilter> pageQueryProvider)
        {
            _mapper = mapper;
            _summaryPriceAccessor = summaryPriceAccessor;
            _pageQueryProvider = pageQueryProvider;
        }

        public async Task<PageWrapper<SummaryPriceItemDto>> GetPageAsync(
            PageContext<SummaryPriceItemFilter> pageContext)
        {
            var priceItemsQuery = _summaryPriceAccessor.All.AsQueryable();
            var result = await Task.Run(() =>
            {
                priceItemsQuery = _pageQueryProvider.Filter(priceItemsQuery, pageContext.Filter);
                priceItemsQuery = _pageQueryProvider.Sort(priceItemsQuery, pageContext.SortList);
                var count = priceItemsQuery.Count();
                var results = priceItemsQuery.Skip(pageContext.Skip)
                    .Take(pageContext.Take).ToArray();

                return new PageWrapper<SummaryPriceItemDto>
                {
                    Count = results.Length,
                    Data = _mapper.Map<SummaryPriceItemDto[]>(results),
                    Offset = pageContext.Skip,
                    TotalCount = count
                };
            });
            return result;
        }

        public async Task<SummaryPriceDictionaryDto> GetAllDictionariesAsync()
        {
            var result =
                await Task.Run(() => _mapper.Map<SummaryPriceDictionaryDto>(_summaryPriceAccessor.Dictionaries));
            return result;
        }

        public SummaryPriceItemDto[] GetAll()
        {
            return _mapper.Map<SummaryPriceItemDto[]>(_summaryPriceAccessor.All);
        }
    }
}
