﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Medex.Abstractions.Business;
using Medex.Abstractions.Persistence;
using Medex.Data.Dto;
using Medex.Data.Dto.Filtering;
using Medex.Domains.Models;
using Medex.Service.Common;
using Microsoft.EntityFrameworkCore;

namespace Medex.Service.Business
{
    public class DistributorPriceService :
        BaseRestService<DistributorPrice, DistributorPriceDto, DistributorPriceFilter>, IDistributorPriceService
    {
        private readonly ISummaryPriceAccessor _summaryPriceAccessor;

        public DistributorPriceService(
            IApplicationDbContext dbContext,
            IMapper mapper,
            ISummaryPriceAccessor summaryPriceAccessor) : base(dbContext, mapper)
        {
            _summaryPriceAccessor = summaryPriceAccessor;
        }

        public override async Task<DistributorPriceDto> CreateAsync(DistributorPriceDto dto)
        {
            dto.Status = Data.Primitives.EnumPriceStatusCode.New;
            var distributorPrices = await _dbContext.DistributorPrices.Where(x => x.DistributorId == dto.DistributorId)
                .ToListAsync();
            _dbContext.DistributorPrices.RemoveRange(distributorPrices);
            var (priceItems, distributors) = await LoadPriceAsync(dto);
            _summaryPriceAccessor.AddPriceList(priceItems, distributors);
            return await base.CreateAsync(dto);
        }

        public override async Task<IList<DistributorPriceDto>> GetAllAsync(DistributorPriceFilter filter = null)
        {
            var results = await _dbContext.DistributorPrices.Include(x => x.Distributor)
                .ProjectTo<DistributorPriceDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
            return results;
        }

        public async Task<DistributorPriceDto> UpdatePriceAsync(DistributorPriceUpdateDto data)
        {
            var price = await _dbContext.DistributorPrices.FirstOrDefaultAsync(x => x.Id == data.Id);
            price.PublicDate = data.PublicDate;
            await _dbContext.SaveChangesAsync();
            return await GetDtoByIdAsync(data.Id);
        }

        public async Task<DistributorPriceDto> ChangePriceStatusAsync(ChangeDistributorPriceStatusDto data)
        {
            var price = await _dbContext.DistributorPrices.FirstOrDefaultAsync(x => x.Id == data.Id);
            price.Status = (int)data.Status;
            await _dbContext.SaveChangesAsync();
            return await GetDtoByIdAsync(data.Id);
        }

        public async Task<bool> RemovePriceAsync(long distributorPriceId)
        {
            var distributorPrice =
                await _dbContext.DistributorPrices.FirstOrDefaultAsync(x => x.Id == distributorPriceId);

            if (distributorPrice != null)
            {
                _dbContext.Remove(distributorPrice);
                if (distributorPrice.DistributorId != null)
                {
                    _summaryPriceAccessor.RemoveByDistributorId(distributorPrice.DistributorId.Value);
                }

                await _dbContext.SaveChangesAsync();
            }

            return true;
        }

        class ProductInfo
        {
            public string Name { get; set; }
            public string AtcClassCodeName { get; set; }
            public string PharmGroupName { get; set; }
        }

        private async Task<(List<SummaryPriceItem>, HashSet<long>)> LoadPriceAsync(DistributorPriceDto price)
        {
            var decimalSeparator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            var document = await _dbContext.Documents.FirstOrDefaultAsync(x => x.Id == price.DocumentId);
            var products = await _dbContext.Products.Select(x => new ProductInfo
                {
                    Name = x.Name,
                    AtcClassCodeName = x.AtcClassCode.Name,
                    PharmGroupName = x.PharmGroup.Name
                })
                .ToArrayAsync();

            var productsDict = new Dictionary<string, ProductInfo>();
            var distributorsResult = new HashSet<long>();

            foreach (var product in products)
            {
                if (!productsDict.ContainsKey(product.Name))
                {
                    productsDict.Add(product.Name, product);
                }
            }

            var priceItems = new List<SummaryPriceItem>();
            var now = DateTimeOffset.Now;
            var allDistributors = await _dbContext.Distributors.ToListAsync();
            using (var ms = new MemoryStream(document.Data))
            {
                using (SpreadsheetDocument doc = SpreadsheetDocument.Open(ms, false))
                {
                    WorkbookPart workbookPart = doc.WorkbookPart;
                    Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
                    Sheet sheet = (Sheet)sheets.FirstOrDefault();
                    Distributor distributor = null;
                    var prevDistributorName = "";
                    if (sheet != null)
                    {
                        Worksheet workSheet = ((WorksheetPart)workbookPart.GetPartById(sheet.Id)).Worksheet;

                        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                        foreach (var openXmlElement in sheetData)
                        {
                            var row = (Row)openXmlElement;
                            var cells = new List<string>();

                            try
                            {
                                foreach (var openXmlElement1 in row)
                                {
                                    var cell = (Cell)openXmlElement1;
                                    if (cell.DataType != null)
                                    {
                                        if (cell.DataType == CellValues.SharedString)
                                        {
                                            int id;
                                            if (int.TryParse(cell.InnerText, out id))
                                            {
                                                SharedStringItem item = workbookPart.SharedStringTablePart
                                                    .SharedStringTable
                                                    .Elements<SharedStringItem>().ElementAt(id);
                                                cells.Add(item.InnerText);
                                            }
                                            else
                                            {
                                                cells.Add(cell.InnerText);
                                            }
                                        }
                                        else
                                        {
                                            cells.Add(cell.InnerText);
                                        }
                                    }
                                    else
                                    {
                                        cells.Add(cell.InnerText);
                                    }
                                }

                                if (cells.Count < 11)
                                {
                                    break;
                                }

                                if (string.IsNullOrEmpty(cells[0]))
                                {
                                    break;
                                }

                                string productName = cells[0]?.Trim();
                                string interName = cells[1]?.Trim();
                                string groupName = cells[2]?.Trim();
                                string expiredDateStr = cells[7]?.Trim();
                                string manufacturerName = cells[9]?.Trim();
                                string distributorName = cells[8]?.Trim();
                                if (prevDistributorName != distributorName)
                                {
                                    prevDistributorName = distributorName;
                                    distributor =
                                        allDistributors.FirstOrDefault(
                                            x => x.Name == distributorName);
                                    if (distributor != null)
                                    {
                                        distributorsResult.Add(distributor.Id);
                                    }
                                }

                                if (distributor == null)
                                {
                                    continue;
                                }

                                string country = cells[10]?.Trim();
                                string actualPriceDateStr = cells.Count > 11 ? cells[11]?.Trim() : null;


                                decimal margin;
                                margin = string.IsNullOrEmpty(cells[3])
                                    ? 0
                                    : (decimal.TryParse(cells[3].Replace("%", "")
                                        .Replace(".", decimalSeparator).Replace(",", decimalSeparator), out margin)
                                        ? margin
                                        : 0);

                                decimal cost;
                                cost = decimal.TryParse(
                                    cells[4].Replace(".", decimalSeparator).Replace(",", decimalSeparator),
                                    out cost)
                                    ? cost
                                    : -1;

                                cost = string.IsNullOrEmpty(cells[4]) || cells[4].StartsWith("догов")
                                    ? -1
                                    : (cells[4].StartsWith("ожидаемый") ? -2 : cost);

                                decimal costDollar;
                                costDollar =
                                    decimal.TryParse(
                                        cells[5].Replace(".", decimalSeparator).Replace(",", decimalSeparator),
                                        out costDollar)
                                        ? costDollar
                                        : -1;
                                costDollar = string.IsNullOrEmpty(cells[5]) || cells[5].StartsWith("догов")
                                    ? -1
                                    : (cells[5].StartsWith("ожидаемый") ? -2 : costDollar);

                                decimal costEuro;
                                costEuro = decimal.TryParse(
                                    cells[6].Replace(".", decimalSeparator).Replace(",", decimalSeparator),
                                    out costEuro)
                                    ? costEuro
                                    : -1;

                                costEuro = string.IsNullOrEmpty(cells[6]) || cells[6].StartsWith("догов")
                                    ? -1
                                    : cells[6].StartsWith("ожидаемый")
                                        ? -2
                                        : costEuro;


                                var priceItem = new SummaryPriceItem
                                {
                                    Cost = cost,
                                    CostInDollar = costDollar,
                                    CostInEuro = costEuro,
                                    Country = country,
                                    Distributor = distributor.Name,
                                    Manufacturer = manufacturerName,
                                    ProductName = productName,
                                    CreatedOn = now,
                                    Margin = margin,
                                    DistributorId = distributor.Id,
                                    GroupName = groupName,
                                    InterName = interName,
                                    PharmGroup = productsDict.ContainsKey(productName)
                                        ? productsDict[productName].PharmGroupName
                                        : null,
                                    AtcClassCode = productsDict.ContainsKey(productName)
                                        ? productsDict[productName].AtcClassCodeName
                                        : null,
                                };
                                //expired date
                                if (DateTime.TryParse(expiredDateStr, out DateTime expiredDate))
                                {
                                    priceItem.ExpiredDate = expiredDate;
                                }
                                else
                                {
                                    priceItem.ExpiredDate = null;

                                    if (double.TryParse(expiredDateStr, out double dateNum))
                                    {
                                        priceItem.ExpiredDate = DateTime.FromOADate(dateNum);
                                    }
                                }

                                // price date
                                if (DateTimeOffset.TryParse(actualPriceDateStr, out DateTimeOffset actualPriceDate))
                                {
                                    priceItem.PublicDate = actualPriceDate;
                                }
                                else
                                {
                                    priceItem.PublicDate = double.TryParse(actualPriceDateStr, out double dateNum)
                                        ? DateTime.FromOADate(dateNum)
                                        : price.PublicDate;
                                }

                                priceItems.Add(priceItem);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                        }
                    }
                }
            }

            return (priceItems, distributorsResult);
        }
    }
}
