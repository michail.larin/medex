﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Medex.Abstractions.Business;
using Medex.Abstractions.Models;
using MimeKit;
using System.Threading.Tasks;
using Medex.Service.Settings;
using Microsoft.Extensions.Options;

namespace Medex.Service.Business
{
    public class EmailService : IEmailService
    {
        private readonly EmailSettings _emailSettings;

        public EmailService(IOptions<EmailSettings> settings)
        {
            _emailSettings = settings.Value;
        }

        public async Task SendAsync(EmailTemplate template)
        {
            
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(template.From));
            foreach(var address in template.To)
            {
                email.To.Add(MailboxAddress.Parse(address));
            }
            email.Subject = template.Subject;
            using (var smtp = new SmtpClient())
            {
                smtp.Connect(_emailSettings.SmtpServer, _emailSettings.Port, SecureSocketOptions.StartTls);
                smtp.Authenticate(_emailSettings.UserName, _emailSettings.Password);
                await smtp.SendAsync(email);
                smtp.Disconnect(true);
            }
        }
    }
}
