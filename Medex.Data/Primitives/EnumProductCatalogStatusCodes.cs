﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Medex.Data.Primitives
{
    public enum EnumProductCatalogStatusCodes
    {
        /// <summary>
        /// Новый
        /// </summary>
        [Display(Name = "Новый")]
        New = 0,
        /// <summary>
        /// На обработке
        /// </summary>
        [Display(Name = "На обработке")]
        Processing = 1,
        /// <summary>
        /// Ошибка обработки
        /// </summary>
        [Display(Name = "Ошибка обработки")]
        ErrorProcessing = 2,
        /// <summary>
        /// Не активный
        /// </summary>
        [Display(Name = "Обработан")]
        Completed = 3
    }
}
