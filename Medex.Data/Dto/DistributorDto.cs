﻿using Medex.Data.Dto.Base;

namespace Medex.Data.Dto
{
    /// <summary>
    /// Поставщик
    /// </summary>
    public class DistributorDto : BaseEntityDto
    {
        /// <summary>
        /// Название поставщика
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Юридическое название
        /// </summary>

        public string LegalName { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }
        
        /// <summary>
        /// Факс
        /// </summary>
        public string Fax { get; set; }
        
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Реквизиты
        /// </summary>
        public string Details { get; set; }
    }
}
