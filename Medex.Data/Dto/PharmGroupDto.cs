﻿using Medex.Data.Dto.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Data.Dto
{
    public class PharmGroupDto : BaseEntityDto
    {
        /// <summary>
        /// Название фарм группы
        /// </summary>
        public string Name { get; set; }
    }
}
