﻿using Medex.Data.Dto.Base.Filtering;

namespace Medex.Data.Dto.Filtering
{
    public class DistributorFilter : BaseFilter
    {
        public FilterDescriptor<long> Id { get; set; }

        public FilterDescriptor<string> Name { get; set; }

        public FilterDescriptor<string> LegalName { get; set; }

        public FilterDescriptor<string> Phone { get; set; }

        public FilterDescriptor<string> Fax { get; set; }

        public FilterDescriptor<string> Email { get; set; }

        public FilterDescriptor<string> Address { get; set; }

        public FilterDescriptor<string> Details { get; set; }
    }
}
