﻿using Medex.Data.Dto.Base.Filtering;
using System;

namespace Medex.Data.Dto.Filtering
{
    public class SummaryPriceItemFilter : BaseFilter
    {
        public FilterDescriptor<string> ProductName { get; set; }

        public FilterDescriptor<string> Manufacturer { get; set; }

        public FilterDescriptor<string> GroupName { get; set; }

        public FilterDescriptor<string> AtcClassCode { get; set; }

        public FilterDescriptor<string> PharmGroup { get; set; }

        public FilterDescriptor<string> Distributor { get; set; }

        public FilterDescriptor<string> Country { get; set; }

        public FilterDescriptor<string> InterName { get; set; }

        public FilterDescriptor<decimal?> Cost { get; set; }

        public FilterDescriptor<decimal?> CostInDollar { get; set; }

        public FilterDescriptor<decimal?> CostInEuro { get; set; }

        public FilterDescriptor<DateTime?> PublicDate { get; set; }
        
        public FilterDescriptor<DateTimeOffset?> ExpiredDate { get; set; }
    }
}
