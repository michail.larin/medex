﻿using Medex.Data.Dto.Base.Filtering;
using System;

namespace Medex.Data.Dto.Filtering
{
    public class DistributorPriceFilter : BaseFilter
    {
        public FilterDescriptor<DateTime?> PublicDate { get; set; }

        public FilterDescriptor<long> DistributorId { get; set; }
    }
}
