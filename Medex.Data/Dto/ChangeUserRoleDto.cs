﻿using Medex.Data.Dto.Base;
using System;
using System.Collections.Generic;
using System.Text;
using Medex.Data.Primitives;

namespace Medex.Data.Dto
{
    public class ChangeUserRoleDto : BaseEntityDto
    {
        public EnumRoleCodes UserRole { get; set; }
    }
}
