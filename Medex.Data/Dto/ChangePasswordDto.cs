﻿using Medex.Data.Dto.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Data.Dto
{
    public class ChangePasswordDto : BaseDto
    {
        public string Passord { get; set; }
        public string ConfirmPassword { get; set; }
    }
}