﻿using Medex.Data.Dto.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Data.Dto
{
    public class AtcClassCodeDto: BaseEntityDto
    {
        /// <summary>
        /// Название класс кода
        /// </summary>
        public string Name { get; set; }
    }
}
