﻿using System.Collections.Generic;

namespace Medex.Data.Dto
{
    public class SummaryPriceDictionaryDto
    {
        public List<string> Products { get; set; }

        public List<string> GroupNames { get; set; }

        public List<string> PharmGroups { get; set; }

        public List<string> InterNames { get; set; }

        public List<string> AtcClassCodes { get; set; }

        public List<string> Countries { get; set; }

        public List<string> Manufacturers { get; set; }

        public List<string> Distributors { get; set; }
    }
}
