﻿using Medex.Data.Dto.Base;
using Medex.Data.Primitives;
using System;

namespace Medex.Data.Dto
{
    /// <summary>
    /// Каталог продуктов
    /// </summary>
    public class ProductCatalogDto : BaseEntityDto
    {
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedOn { get; set; }

        public EnumProductCatalogStatusCodes Status { get; set; }

        public DocumentDto Document { get; set; }

        public long DocumentId { get; set; }
    }
}
