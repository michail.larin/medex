﻿using System;
using Medex.Data.Dto.Base;
using Medex.Data.Primitives;

namespace Medex.Data.Dto
{
    public class DistributorPriceDto : BaseEntityDto
    {
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Дата публикации сводного прайслиста
        /// </summary>
        public DateTime PublicDate { get; set; }

        /// <summary>
        /// Статус прайс листа
        /// </summary>
        public EnumPriceStatusCode Status { get; set; }

        public DocumentDto Document { get; set; }

        public long DocumentId { get; set; }
        
        public long? DistributorId { get; set; }
        
        public DistributorDto Distributor { get; set; }
    }
}
