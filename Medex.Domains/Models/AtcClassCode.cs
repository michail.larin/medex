﻿using Medex.Domains.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Domains.Models
{
    public class AtcClassCode:BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
