﻿using Medex.Domains.Models.Base;
using System;
using System.Collections.Generic;

namespace Medex.Domains.Models
{
    public class DistributorPrice : BaseEntity
    {
        public long? DistributorId { get; set; }
        
        public DateTime CreatedOn { get; set; }
        
        public DateTime PublicDate { get; set; }
        
        public int Status { get; set; }
                
        public long? DocumentId { get; set; }
        
        public virtual Document Document { get; set; }
        
        public virtual Distributor Distributor { get; set; }
        
        public virtual List<DistributorPriceItem> DistributorPriceItems { get; set; }

    }
}
