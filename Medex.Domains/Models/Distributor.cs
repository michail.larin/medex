﻿using Medex.Domains.Models.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Medex.Domains.Models
{
    [Table("Distributors")]
    public class Distributor : BaseEntity
    {
        /// <summary>
        /// Название поставщика
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Юридическое название
        /// </summary>

        public string LegalName { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Факс
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Реквизиты
        /// </summary>
        public string Details { get; set; }

        /// <summary>
        /// Индиактор Активный/Не активный
        /// </summary>
        public bool IsActive { get; set; }

        public virtual List<PriceItem> PriceItems { get; set; }

        public virtual List<DistributorPrice> DistributorPrices { get; set; }
    }
}
