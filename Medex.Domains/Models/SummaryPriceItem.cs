﻿namespace Medex.Domains.Models
{
    using  System;
    
    public class SummaryPriceItem
    {
        public Guid Id { get; set; }
        public string ProductName { get; set; }

        public string InterName { get; set; }

        public string GroupName { get; set; }

        public string PharmGroup { get; set; }

        public string AtcClassCode { get; set; }

        public string Distributor { get; set; }

        public string Manufacturer { get; set; }

        public string Country { get; set; }

        public decimal Cost { get; set; }

        public decimal CostInDollar { get; set; }

        public decimal CostInEuro { get; set; }

        public decimal Margin { get; set; }

        public long DistributorId { get; set; }
        
        public DateTimeOffset PublicDate { get; set; }

        public DateTimeOffset CreatedOn { get; set; }
        
        public DateTimeOffset? ExpiredDate { get; set; }
    }
}
