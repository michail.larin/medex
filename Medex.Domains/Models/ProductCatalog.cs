﻿using Medex.Domains.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Domains.Models
{
    public class ProductCatalog : BaseEntity
    {
        public long DocumentId { get; set; }

        public int Status { get; set; }

        public DateTime CreatedOn { get; set; }

        public virtual Document Document { get; set; }

    }
}
