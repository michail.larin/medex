﻿using Medex.Domains.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Domains.Models
{
    public class DistributorPriceItem: BaseEntity
    {
        public long DistributorPriceId { get; set; }
        public long ProductId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime Date { get; set; }
        public decimal Cost { get; set; }
        public decimal CostInDollar { get; set; }
        public decimal CostInEuro { get; set; }
        public decimal Margin { get; set; }
        public virtual DistributorPrice DistributorPrice { get; set; }
        public virtual Product Product { get; set; }
    }
}
