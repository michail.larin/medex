﻿using System.Threading.Tasks;

namespace Medex.Abstractions.Common
{
    public interface IProductCatalogLoaderService
    {
        Task LoadProductCatalogAsync(long productCatalogId);
    }
}
