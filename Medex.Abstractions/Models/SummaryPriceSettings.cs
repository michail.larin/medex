﻿namespace Medex.Abstractions.Models
{
    public class SummaryPriceSettings
    {
        public string Sample { get; set; }
        public string Path { get; set; }
    }
}
