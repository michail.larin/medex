﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Abstractions.Models
{
    public class EmailTemplate
    {
        public string From { get; set; }
        public IList<string> To { get; set; }
        public string MessageBody { get; set; }
        public string Subject { get; set; }
    }
}
