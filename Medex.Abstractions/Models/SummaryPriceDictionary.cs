﻿using System.Collections.Generic;

namespace Medex.Abstractions.Models
{
    public class SummaryPriceDictionary
    {
        public string[] Products { get; set; }

        public string[] GroupNames { get; set; }

        public string[] PharmGroups { get; set; }

        public string[] InterNames { get; set; }

        public string[] AtcClassCodes { get; set; }

        public string[] Countries { get; set; }

        public string[] Manufacturers { get; set; }

        public string[] Distributors { get; set; }
    }
}
