﻿using Medex.Abstractions.Common;
using Medex.Data.Dto;
using Medex.Data.Dto.Filtering;
using Medex.Domains.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Abstractions.Business
{
    public interface IAtcClassCodeService : IReadService<AtcClassCode, AtcClassCodeDto, NameFilter>,
        IUpdateService<AtcClassCode, AtcClassCodeDto>,
        ICreateService<AtcClassCode, AtcClassCodeDto>,
        IDeleteService
    {
    }
}
