﻿using System.Collections.Generic;
using Medex.Abstractions.Models;
using Medex.Domains.Models;

namespace Medex.Abstractions.Business
{
    public interface ISummaryPriceAccessor
    {
        void AddPriceList(IList<SummaryPriceItem> summaryPriceItems, HashSet<long> distributorId);
        
        void RemoveByDistributorId(long distributorId);

        List<SummaryPriceItem> All { get; }

        SummaryPriceDictionary Dictionaries { get; }
    }
}
