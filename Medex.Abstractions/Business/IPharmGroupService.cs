﻿using Medex.Abstractions.Common;
using Medex.Data.Dto;
using Medex.Data.Dto.Filtering;
using Medex.Domains.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Abstractions.Business
{
    public interface IPharmGroupService : IReadService<PharmGroup, PharmGroupDto, NameFilter>,
        IUpdateService<PharmGroup, PharmGroupDto>,
        ICreateService<PharmGroup, PharmGroupDto>,
        IDeleteService
    {
    }
}
