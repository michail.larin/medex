﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Medex.Data.Dto;
using Medex.Data.Dto.Base.Paging;
using Medex.Data.Dto.Filtering;

namespace Medex.Abstractions.Business
{
    public interface ISummaryPriceService
    {
        Task<PageWrapper<SummaryPriceItemDto>> GetPageAsync(PageContext<SummaryPriceItemFilter> pageContext);

        Task<SummaryPriceDictionaryDto> GetAllDictionariesAsync();

        SummaryPriceItemDto[] GetAll();

    }
}
