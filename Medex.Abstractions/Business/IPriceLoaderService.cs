﻿using System.Threading.Tasks;

namespace Medex.Abstractions.Business
{
    public interface IPriceLoaderService
    {
        Task LoadPriceAsync(long priceId);
    }
}
