﻿using Medex.Abstractions.Models;
using Medex.Abstractions.Primitives;
using Medex.Domains.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Abstractions.Business
{
    public interface IEmailTemplateFactory
    {
        EmailTemplate Create(EnumEmailTypes emailType,string from, string to, string messageBody, string messageHeader, params object[] parameters);
    }
}
