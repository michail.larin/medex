﻿using Medex.Abstractions.Common;
using Medex.Data.Dto;
using Medex.Data.Dto.Filtering;
using Medex.Domains.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Medex.Abstractions.Business
{
    public interface IDistributorPriceService :
        IReadService<DistributorPrice, DistributorPriceDto, DistributorPriceFilter>,
        ICreateService<DistributorPrice, DistributorPriceDto>,
        IUpdateService<DistributorPrice, DistributorPriceDto>,
        IDeleteService
    {
        Task<DistributorPriceDto> UpdatePriceAsync(DistributorPriceUpdateDto data);

        Task<DistributorPriceDto> ChangePriceStatusAsync(ChangeDistributorPriceStatusDto data);

        Task<bool> RemovePriceAsync(long distributorPriceId);
    }
}
