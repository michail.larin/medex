using System.Threading;
using System.Threading.Tasks;

namespace Medex.Abstractions.Business
{
    public interface IOrganizationService
    {
        Task<string[]> GetAllOrganizationsAsync(CancellationToken cancellationToken);
    }
}
