﻿using Medex.Abstractions.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Medex.Abstractions.Business
{
    public interface IEmailService
    {
        Task SendAsync(EmailTemplate template);
    }
}
