﻿using Medex.Abstractions.Common;
using Medex.Data.Dto;
using Medex.Data.Dto.Filtering;
using Medex.Domains.Models;
using System.Threading.Tasks;

namespace Medex.Abstractions.Business
{
    public interface IProductCatalogService : IReadService<ProductCatalog, ProductCatalogDto, NameFilter>,
        ICreateService<ProductCatalog, ProductCatalogDto>,
        IUpdateService<ProductCatalog, ProductCatalogDto>,
        IDeleteService
    {
        public Task<ProductCatalogDto[]> GetAllNewProductCatalogsAsync();
    }
}
