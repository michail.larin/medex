﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Medex.Abstractions.Primitives
{
    public enum EnumEmailTypes
    {
        ForgotPassword = 0,
        EmailConfirm = 1,
        Invite = 2,
        Response = 3,
        Faq = 4,
    }
}
